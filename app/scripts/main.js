var scene = document.getElementById('scene');
var parallax = new Parallax(scene, {
    calibrateX: false,
    calibrateY: true,
    invertX: false,
    invertY: false,
    limitX: 70,
    limitY: 30,
    scalarX: 60,
    scalarY: 60,
    frictionX: 0.1,
    frictionY: 0.1
});

$('.thumbnail-photo').click(function(){
    'use strict';
    // show overlay view
    var url = $(this).children('img').attr('src');
    $('.overlay-gallery img').attr('src',url);
    $('.overlay-gallery').css('display', 'block');

    // lock scroll position, but retain settings for later
    var aboutY = $('#about').offset().top;
    var actualY = document.documentElement.scrollTop  || document.body.scrollTop;
    var scrollPosition = [0, actualY];
    var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
    html.data('scroll-position', scrollPosition);
    html.data('previous-overflow', html.css('overflow'));
    html.css('overflow', 'hidden');
    
    window.scrollTo(0, aboutY);
});


$('.overlay-gallery img').click(function(){
    'use strict';
    // hide overlay view
    $('.overlay-gallery').css('display', 'none');

    // un-lock scroll position
    var html = jQuery('html');
    var scrollPosition = html.data('scroll-position');
    html.css('overflow', html.data('previous-overflow'));
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
});


$('nav ul li').click(function(){
    'use strict';
    //Remove previous active class
    $('nav ul li.active').removeClass('active');

    //Add active class to the current option
    $(this).addClass('active');
});


//setting min-height so that the articles have the same height
var $articles = $('.hidden-articles article');
var count = $articles.length;
var maxHeight = 0;
var i =0;
for (;i < count;i++) {
    var articleHeight = $($articles.get(i)).height();
    if (maxHeight < articleHeight) {
        maxHeight = articleHeight;
    }
}

var j =0;
for (;j < count;j++) {
    $($articles.get(j)).css('height',maxHeight + 10);
}